package com.ejemplo.modelos;

import java.util.Date;

public class Desarrollador {
	private String usuario, password, detalles, genero, rol, firma;
	private String[] lenguajesProgramacion;
	private boolean disponible;
	private Date fechaNacimiento;
	
		public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDetalles() {
		return detalles;
	}
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public String getFirma() {
		return firma;
	}
	public void setFirma(String firma) {
		this.firma = firma;
	}
	public String[] getLenguajesProgramacion() {
		return lenguajesProgramacion;
	}
	public void setLenguajesProgramacion(String[] lenguajesProgramacion) {
		this.lenguajesProgramacion = lenguajesProgramacion;
	}
	public boolean isDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	
}
