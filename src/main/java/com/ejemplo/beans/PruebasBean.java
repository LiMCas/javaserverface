package com.ejemplo.beans;


import java.util.Map;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.bean.ManagedBean;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;

@RequestScoped
@Named(value="prueba")
public class PruebasBean {
	private String[] lenguajes = {"2","4"};
	private String seleccion;

	public String getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}

	public String[] getLenguajes() {
		return lenguajes;
	}

	public void setLenguajes(String[] lenguajes) {
		this.lenguajes = lenguajes;
	} 
	
	public String mostrarValor() {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> parametros = fc.getExternalContext().getRequestParameterMap();
		seleccion = parametros.get("val");
		return "respuesta";
	}
}
