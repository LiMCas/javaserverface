package com.ejemplo.beans;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

@RequestScoped
@Named
public class HolaBean {//holaBean
	private String nombre, saludo;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSaludo() {
		return saludo;
	}
	
	public void armarSaludo() {
		saludo = "Hola Sr. " + nombre;
	}
	
}
