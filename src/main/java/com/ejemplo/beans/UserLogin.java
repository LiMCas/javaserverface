package com.ejemplo.beans;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

@RequestScoped
@Named
public class UserLogin {
	private String message = "Enter username and password.";
	private String username;
	private String password;

	public String login() {
		if ("admin".equalsIgnoreCase(username) && "admin".equalsIgnoreCase(password)) {
			message = "Successfully logged-in.";
			return "success";
		} else {
			message = "Wrong credentials.";
			return "login";
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
