package com.ejemplo.beans;

import com.ejemplo.modelos.Desarrollador;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

@RequestScoped
@Named
public class GestionaDesarrolladorBean {
	private Desarrollador desarrollador;
	
	

	public GestionaDesarrolladorBean() {
		super();
		// TODO Auto-generated constructor stub
		desarrollador = new Desarrollador();
		desarrollador.setGenero("F"); //F=Femenino, M=Masculino
		desarrollador.setDisponible(true);
		desarrollador.setLenguajesProgramacion(new String[] {"1","3"});
	}

	public Desarrollador getDesarrollador() {
		return desarrollador;
	}

	public void setDesarrollador(Desarrollador desarrollador) {
		this.desarrollador = desarrollador;
	} 
	
	public String registrar() {
		return "registrar"
	}
	
}
